#!/bin/sh

filename=$1
filepwd=$(pwd)

echo split file
rm -f 000_*
split -b 90m $filename 000_

echo git clone dev branch
rm -rf transgit
git clone -b dev git@gitee.com:missu/transgit.git
cd transgit
tmpbranch=tmp-$(date +"%Y%m%d-%H%M%S")
git checkout -b $tmpbranch
git push origin "$tmpbranch":"$tmpbranch"

cd $filepwd

echo foreach 000_ files
for i in $(ls -1 000_*|xargs)
do
  cd $filepwd
  cd transgit/

  echo cp file $i
  cp ../$i $i.tar.gz
  ls -1

  echo git add commit push
  git status
  git add . 
  git commit -m  "$i"
  git push -u origin "$tmpbranch":"$tmpbranch" 

done